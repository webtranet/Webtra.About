__          __  _     _                        _                 _   
\ \        / / | |   | |                 /\   | |               | |  
 \ \  /\  / /__| |__ | |_ _ __ __ _     /  \  | |__   ___  _   _| |_ 
  \ \/  \/ / _ \ '_ \| __| '__/ _` |   / /\ \ | '_ \ / _ \| | | | __|
   \  /\  /  __/ |_) | |_| | | (_| |_ / ____ \| |_) | (_) | |_| | |_ 
    \/  \/ \___|_.__/ \__|_|  \__,_(_)_/    \_\_.__/ \___/ \__,_|\__|

-------------------------------------------------------------------------------

  What is it?
  -----------

  Webtra.About is a TripleTower wapp which offers information about Webtra.net,
  as well as contact and version information. Webtra.About is open but not
  free software based on web technologies, open source software and the
  tripletower-sdk.


  The Latest Version
  ------------------

  Details of the latest version can be found on the Webtranet Affinity Groups's
  Gitlab repository: https://gitlab.com/webtranet/Webtra.About


  Documentation
  -------------

  The documentation available as of the date of this release can be found on
  the Webtranet Affinity Group's website in the documentation section:
  https://webtranet.online/wappstower/Webtra.Docs


  Installation
  ------------

  We recommend using the hanoi software installer which is currently available
  for linux and windows in the tripletower-sdk.


  Licensing
  ---------

  Please see the file called LICENSE.


  Contacts
  --------

     o If you would like to purchase support for running TripleTower please
       write a mail to `sales@webtranet.online´ so we can get in touch with you
       and write you an offering.

     o If you want to participate in actively developing TripleTower please
       write a mail to `contribute@webtranet.online´ to get information about a
       Contributor License Agreement (CLA).