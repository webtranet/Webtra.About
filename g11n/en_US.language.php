<?php declare(strict_types=1); return
[
	"WEBTRA.ABOUT.VERSION" => "Version %1\$s - %2\$s",
	"WEBTRA.ABOUT.CONTACTS" => "The Webtra.net affity group is always open for feedback, bug reports and feature requests.<br /> Simply write a mail to <a href=\"mailto:info@webtranet.online?Subject=Webtra.net feedback\" target=\"_top\">info@webtranet.online</a>. <br />Happy hacking."
];