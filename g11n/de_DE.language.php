<?php declare(strict_types=1); return
[
	"WEBTRA.ABOUT.VERSION" => "Version %1\$s - %2\$s",
	"WEBTRA.ABOUT.CONTACTS" => "Die Webtra.net affity group ist immer offen für Feedback, Bugreports und Featurerequests.<br /> Einfach eine Mail an <a href=\"mailto:info@webtranet.online?Subject=Webtra.net Feedback\" target=\"_top\">info@webtranet.online</a> schicken. <br />Happy hacking."
];